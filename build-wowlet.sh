## Make sure we are compiling from musl
##
libc=$(ldd /bin/ls | grep 'musl' | head -1 | cut -d ' ' -f1)
if [ -z $libc ]; then
    # This is not Musl.
    echo "this is not musl"
    exit 1
fi
echo "this is musl"

## Install Wowlet build Depends
##
sudo xbps-install -SuAy gnutls-devel qrencode-devel \
libX11-devel cairo-devel qt5-declarative-devel \
qt5-quickcontrols2-devel qt5-svg-devel \
qt5-multimedia-devel qt5-websockets-devel \
czmq-devel libsodium-devel libXinerama-devel \
libXfixes-devel libzbar-devel libgcrypt-devel \
libzip-devel

## Get and compile/install wownero-seed
##
git clone https://git.wownero.com/wowlet/wownero-seed external/wownero-seed
cd external/wownero-seed/src
cmake ../.
sudo make install
cd ../../..

## Get and compile wowlet + wownero cli
##
git clone https://git.wownero.com/wowlet/wowlet external/wowlet
cd external/wowlet
git submodule update --init --recursive
cmake -DMANUAL_SUBMODULES=1 -DUSE_DEVICE_TREZOR=OFF -DUSE_SINGLE_BUILDDIR=ON -DDEV_MODE=ON -DSTACK_TRACE=OFF -DARCH=x86-64
make
cp bin/* ../../include/usr/bin
cp src/assets/images/appicons/256x256.png ../../include/usr/share/icons/wowlet.png
cp src/assets/org.wowlet.wowlet.desktop ../../include/etc/skel/.local/share/applications/wowlet.desktop
cd ../..
