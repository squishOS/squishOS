##set build hash id
buildHash=$(git rev-parse --short HEAD)

##update submodules
git submodule update --init --recursive --remote

##add hosts from stevenblack/hosts
cp external/hosts/hosts include/etc/hosts

##add dracula theme from dracula/gtk
cp -a external/gtk include/usr/share/themes/dracula

##add locked_user.js from pyllyukko/user.js
cd external/user.js
make locked_user.js
cp locked_user.js ../../include/usr/lib/firefox/mozilla.cfg
cd ../..

##build feather
./build-feather.sh

##build wowlet
./build-wowlet.sh

##build squishOS with sudo
cd external/void-mklive
make
sudo ./mklive.sh \
-a x86_64-musl \
-I ../../include \
-o ../../squishOS-x86_64-$buildHash.iso \
-C "live.autologin live.user=squish live.shell=/bin/fish" \
-S "dbus lightdm NetworkManager ntpd polkitd tor i2pd" \
-p "dialog cryptsetup lvm2 mdadm xtools-minimal xmirror \
espeakup void-live-audio brltty xorg-minimal xorg-input-drivers \
xorg-video-drivers setxkbmap xauth font-misc-misc terminus-font \
dejavu-fonts-ttf orca lightdm lightdm-gtk3-greeter xfce4 \
gnome-themes-standard gnome-keyring network-manager-applet \
gvfs-afc gvfs-mtp gvfs-smb udisks2 xfce4-pulseaudio-plugin \
firefox-esr seahorse pwgen keepassxc openntpd electrum \
macchanger gnome-disk-utility runit-iptables gnupg2 dino dillo \
curl vlc qt5-websockets boost-devel libzip tor i2pd \
czmq qt5-quickcontrols2 fish-shell kleopatra"
